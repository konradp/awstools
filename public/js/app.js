const templates = [
  'manual-example',
  'two',
];
let gJson = null;

window.onload = function() {
  // Init
  console.log('test');
  const json = _getId('json');
  json.innerHTML = 'trololo';

  // Templates
  const opt_templates = _getId('template');
  for (template of templates) {
    const opt = _new('option');
    opt.innerHTML = template;
    opt_templates.appendChild(opt);
  }
  const btn_template_apply = _getId('apply-template');
  btn_template_apply.addEventListener('click', onApplyTemplate);

  onApplyTemplate();
}

function onApplyTemplate() {
  const template = _getId('template').value;
  console.log(template);
  fetch('templates/' + template + '.json')
    .then((response => response.json()))
    .then(data => {
      console.log(data);
      const json = _getId('json');
      gJson = data;
      json.innerHTML = JSON.stringify(data, null, 2);
      // TODO: Analyse the json and add buttons on the left
      Analyse();
    })
}

function Analyse() {
  // Analyse JSON
  const left = _getId('left');
  left.innerHTML = '';

  // Name, roleArn
  _newInput('name', 'name:', gJson.pipeline.name, left);
  _newInput('role-arn', 'roleArn:', gJson.pipeline.roleArn, left);

  // stages
  let stages = _new('div');
  stages.style = 'background-color:white';
  stages.innerHTML = 'stages:';
  left.appendChild(stages);
  //for (stage of gJson.pipeline.stages) {
  let objStages = gJson.pipeline.stages;
  for (let i_stage = 0; i_stage < objStages.length; i_stage++) {
    // stage
    let addr = `pipeline.stages[${i_stage}]`;
    console.log('addr', addr);
    let stage = objStages[i_stage];
    let div_stage = _new('div');
    div_stage.className = 'section';
    stages.appendChild(div_stage);
    //_newInput(stage.name, 'name:', stage.name, div_stage);
    _newInput(`${addr}.name`, 'name:', stage.name, div_stage);

    // actions
    let actions = _new('div');
    actions.innerHTML = 'actions:';
    div_stage.appendChild(actions);
    let objActions = stage.actions;
    //for (action of stage.actions) {
    for (let i_action = 0; i_action < objActions.length; i_action++) {
      // action
      let action = objActions[i_action];
      let addr_action = `${addr}.actions[${i_action}]`;
      let div_action = _new('div');
      div_action.className = 'section';
      actions.appendChild(div_action);
      //_newInput(action.name, 'name:', action.name, div_action);
      _newInput(addr_action, 'name:', action.name, div_action);
    }
  }
}

function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}
function _newInput(id, label, value, parent_div) {
  let lbl = _new('label');
  lbl.innerHTML = label;
  let input = _new('input');
  input.id = id;
  input.value = value;
  input.addEventListener('input', (e) => {
    console.log('a', e.target.value);
    value = e.target.value;
    console.log('target id', id);
    set(gJson, id, e.target.value);
    //console.log(gJson);
    // TODO: Could this be better?
    if (id == 'name') {
      gJson.pipeline.name = e.target.value;
    }
    RenderJson();
  })

  let div = _new('div');
  div.style = 'display:flex';
  div.appendChild(lbl);
  div.appendChild(input);
  parent_div.appendChild(div);
}
function RenderJson() {
  const json = _getId('json');
  json.innerHTML = JSON.stringify(gJson, null, 2);
}

// https://stackoverflow.com/questions/10934664/convert-string-in-dot-notation-to-get-the-object-reference
function set(obj, str, val) {
  console.log('str', str);
  str = str.split('.');
  while (str.length > 1)
    obj = obj[str.shift()];
    console.log('-----', 'str', str, 'obj', obj);
  return obj[str.shift()] = val;
}

//function ref(obj, str) {
//  str = str.split('.');
//  for (var i = 0; i < str.length; i++)
//    obj = obj[str[i]];
//  return obj;
//}
